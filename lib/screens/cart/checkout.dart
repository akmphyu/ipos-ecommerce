import 'package:flutter/material.dart';
import "../../widgets/checkout_product.dart";
import "../../widgets/checkout_address.dart";
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:provider/provider.dart';
import '../../providers/cart.dart';

class CheckOutPage extends StatefulWidget {
  @override
  _CheckOutPageState createState() => _CheckOutPageState();
}

class _CheckOutPageState extends State<CheckOutPage> {
  Map<String, dynamic> profiles;
  getData() async {
    http.Response response = await http.get(
        Uri.encodeFull(
            "http://uat.ecommerce.winspecgroup.com/v2/customer-info/get-customer-info"),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization':
              'Bearer YmVhcmVyIGpqaHNqZ2hram9zcGdmc2hpeUFHREZJSE1TS0JOSkJTRFZQQUtPRVI1NDg1'
        });
    this.setState(() {
      profiles = json.decode(response.body);
//      profileList = profiles['data'];
    });
  }

  @override
  void initState() {
    // TODO: implement initState

    this.getData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final carts = Provider.of<Cart>(context).orderItems;
    final totalCost = Provider.of<Cart>(context).totalCost;
    return Scaffold(
      appBar:
          AppBar(title: Text("Shopping Cart"), backgroundColor: Colors.orange),
      body: Column(
        children: [
          CheckoutAddress(
            profiles: profiles,
          ),

          Expanded(
            child: ListView.builder(
              itemBuilder: (ctx, index) => Flexible(
                child: CheckoutProduct(
                  productCode: carts[index].productCode,
                  productDesc: carts[index].productDesc,
                  productQty: carts[index].productQty,
                  productPrice: carts[index].productPrice,

                ),
              ),
              itemCount: carts.length,
            ),
          ),
        ],
      ),
      bottomNavigationBar: Container(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Text("Total Payment : \$ ${totalCost}"),
            SizedBox(
              width: 5,
            ),
            RaisedButton(
              child: Text('Place Order '),
              color: Colors.yellow,
              onPressed: () {},
            ),
          ],
        ),
      ),

    );
  }
}
