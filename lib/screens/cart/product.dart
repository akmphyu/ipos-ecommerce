import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../providers/cart.dart';
import '../../widgets/badge.dart';
import '../../widgets/cart_product.dart';
import "../../screens/cart/checkout.dart";

class CartProductScreen extends StatefulWidget {
  static const routeName = '/cart-product';
//  final List<String> cartList;
//  CartProductScreen({this.cartList});
  @override
  _CartProductScreenState createState() => _CartProductScreenState();
}

class _CartProductScreenState extends State<CartProductScreen> {
  bool _checkbox = false;
  List<dynamic> cartList = [];

  @override
  Widget build(BuildContext context) {
    final carts = Provider.of<Cart>(context).orderItems;
    final orderCount = Provider.of<Cart>(context).orderItemCount;
    final totalCost = Provider.of<Cart>(context).totalCost;
    return Scaffold(
        //each product have a color

        appBar: AppBar(
            title: Text("Shopping Cart"), backgroundColor: Colors.orange),
        body: Column(children: [
          Expanded(
            child: ListView.builder(
              itemBuilder: (ctx, index) => CartProduct(
                productCode: carts[index].productCode,
                productDesc: carts[index].productDesc,
                productQty: carts[index].productQty,
                checkbox:_checkbox
              ),
              itemCount: carts.length,
            ),
          ),
        ],
        ),
        bottomNavigationBar: Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  Checkbox(
                    value: _checkbox,
                    onChanged: (bool value) {
                      setState(() {
                        _checkbox = !_checkbox;
                      });
                    },
                  ),
                  Text("All"),
                ],
              ),
              Container(
                child: Row(
                  children: [
                    Text("SubTotal"),
                    SizedBox(width: 5,),
                    Text("${totalCost}"),
                    SizedBox(width: 10,),
                    RaisedButton(
                      child: Text('Checkout (${orderCount}) '),
                      color: Colors.yellow,

                      onPressed: (){
                        Navigator.of(context).push(MaterialPageRoute(builder: (_)=>CheckOutPage()));
                      },
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
    );
  }
}
