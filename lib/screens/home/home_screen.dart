import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:ipos_shop/constants.dart';
import '../components/body.dart';
import 'package:provider/provider.dart';
import '../../providers/cart.dart';
import '../../screens/customer/customer_profile.dart';
import '../../screens/customer/customer_checkout_form.dart';
import 'package:http/http.dart' as http;

import 'dart:convert';
class HomeScreen extends StatefulWidget {
   int _selectedPageIndex = 0;
  HomeScreen(this._selectedPageIndex);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  TextEditingController usernameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  bool isAuth = false;

  List<Widget> _pages = [
    Body(),
    CustomerCheckoutForm(),
    CustomerProfile()
  ];
  int _selectedPageIndex = 0;
  void _selectPage(int index){
  setState((){
    _selectedPageIndex = index;
  });
  }
  @override
  Widget build(BuildContext context) {
    final carts = Provider.of<Cart>(context).orderItems;
    return isAuth ? authScreen() : UnauthScreen();
  }

  AppBar buildAppBar() {
    return AppBar(
      backgroundColor: Colors.orange,
      elevation: 0,
      leading: IconButton(
        icon: SvgPicture.asset("assets/icons/back.svg", color: kTextColor),
        onPressed: () {},
      ),
      actions: <Widget>[
        IconButton(
          icon: SvgPicture.asset(
            "assets/icons/search.svg",
            color: kTextColor,
          ),
          onPressed: () {},
        ),
        IconButton(
          icon: SvgPicture.asset("assets/icons/cart.svg", color: kTextColor),
          onPressed: () {},
        ),
        SizedBox(
          width: kDefaultPaddin / 2,
        )
      ],
    );
  }

  Scaffold authScreen() {
    return Scaffold(
//      appBar: mainBanner("AK-shop"),

      body: _pages[_selectedPageIndex],
      bottomNavigationBar: BottomNavigationBar(
          onTap: _selectPage,
          backgroundColor: Colors.orange,
          unselectedItemColor: Colors.white,
          selectedItemColor: Colors.purple,
          currentIndex: _selectedPageIndex,
          items:[
            BottomNavigationBarItem(icon: Icon(Icons.home), title: Text('Feeds'),),
            BottomNavigationBarItem(icon: Icon(Icons.shopping_cart),title: Text('Cart'),),
            BottomNavigationBarItem(icon: Icon(Icons.people),title: Text('Profile'),),
          ]
      ),
    );
  }
  Scaffold UnauthScreen() {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
              colors: [Colors.orange, Colors.deepOrangeAccent],
              begin: Alignment.topLeft,
              end: Alignment.bottomRight),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[textSection(), buttonSection()],
        ),
      ),
    );
  }

  logIn(String username, String password) async {
    http.Response response = await http.get(
        Uri.encodeFull(
            "http://uat.ipos.winspecgroup.com/v2/user/get-user?username=${username}&password=${password}"),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer YmVhcmVyIGpqaHNqZ2hram9zcGdmc2hpeUFHREZJSE1TS0JOSkJTRFZQQUtPRVI1NDg1'
        });
    Map<String, dynamic> users = json.decode(response.body);
    List<dynamic> user = users['data'];
    if (users["status"] == true) {
      this.setState(() {
        isAuth = true;
      });
    } else {
      this.setState(() {
        isAuth = false;
      });
    }
  }

  Container buttonSection() {
    return Container(
      width: (MediaQuery.of(context).size.width - 50),
      height: 40.0,
      margin: EdgeInsets.only(top: 30.0),
      child: RaisedButton(
        onPressed: () =>
            logIn(usernameController.text, passwordController.text),
        color: const Color(0xFF334756),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
        child: Text(
          "Sign In",
          style: TextStyle(color: Colors.white70),
        ),
      ),
    );
  }

  Container textSection() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20.0),
      margin: EdgeInsets.only(top: 30.0),
      child: Column(
        children: <Widget>[
          txtSection("Username", Icons.people, usernameController),
          SizedBox(height: 30.0),
          txtSection("Password", Icons.lock, passwordController)
        ],
      ),
    );
  }

  TextFormField txtSection(
      String title, IconData icon, TextEditingController txtController) {
    return TextFormField(
      controller: txtController,
      style: TextStyle(color: Colors.white70),
      decoration: InputDecoration(
          hintText: title,
          hintStyle: TextStyle(color: Colors.white),
          icon: Icon(icon)),
    );
  }

//  @override
//  Widget build(BuildContext context) {
//    return isAuth ? authScreen() : UnauthScreen();
//  }
}
