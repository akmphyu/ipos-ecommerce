import 'package:flutter/material.dart';
import 'package:ipos_shop/models/Product.dart';
import 'package:ipos_shop/constants.dart';

class ProductTitleWithImage extends StatelessWidget {
  final String productBrand;
  final String productCode;
  final String productCurrency;
  final String productPrice;
  ProductTitleWithImage({this.productBrand, this.productCode, this.productCurrency,this.productPrice});
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: kDefaultPaddin),

      child: Container(
        margin: EdgeInsets.only(top:kDefaultPaddin),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Column(children: [
              Text(
                productBrand,
                style: TextStyle(color: Colors.white),
              ),
              Text(
                productCode,
                style: Theme.of(context)
                    .textTheme
                    .headline5
                    .copyWith(color: Colors.white, fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: kDefaultPaddin,
              ),
              RichText(
                text: TextSpan(children: [
                  TextSpan(text: "Price\n"),
                  TextSpan(
                    text: '${productCurrency} ${productPrice}',
                    style: Theme.of(context).textTheme.headline5.copyWith(
                        color: Colors.white, fontWeight: FontWeight.bold),
                  ),
                ]),
              ),
            ],),

            Row(
              children: [
                SizedBox(
                  width: kDefaultPaddin,
                ),
                Container(
                  width: 150,
                  height: 150,
                  padding: EdgeInsets.all(kDefaultPaddin),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(16)),
                  child: Hero(
                    tag: "${productCode}",
                    child: Image.asset(
                      "assets/images/product_default.jpg",
                      fit: BoxFit.fill,
                    ),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
