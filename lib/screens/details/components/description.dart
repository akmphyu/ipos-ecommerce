import 'package:flutter/material.dart';
import 'package:ipos_shop/models/Product.dart';
import 'package:ipos_shop/constants.dart';

class description extends StatelessWidget {
  const description({
    Key key,
    @required this.product,
  }) : super(key: key);

  final String product;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical:kDefaultPaddin),
      child: Text(
        product,
        style: TextStyle(height: 1.5),
      ),
    );
  }
}
