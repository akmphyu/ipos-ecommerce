import 'package:flutter/material.dart';
import 'package:ipos_shop/constants.dart';
import 'package:flutter_svg/svg.dart';
import 'package:ipos_shop/models/Product.dart';
import 'package:provider/provider.dart';
import '../../../providers/cart.dart';
import '../../../screens/cart/checkout.dart';

class add_to_cart extends StatelessWidget {

  final CartItem carts;
  add_to_cart({this.carts});

  @override
  Widget build(BuildContext context) {
    final cart = Provider.of<Cart>(context, listen:false);

    return Padding(
      padding: const EdgeInsets.symmetric(
          vertical: kDefaultPaddin),
      child: Row(
        children: [
          Container(
            margin: EdgeInsets.only(right: kDefaultPaddin),
            height: 50,
            width: 58,
            decoration: BoxDecoration(
                border: Border.all(color: Colors.blue),
                borderRadius: BorderRadius.circular(18)),
            child: IconButton(
              icon: SvgPicture.asset(
                "assets/icons/add_to_cart.svg", color: Colors.blue,),
              onPressed: () { cart.addOrderItem(carts); },
            ),
          ),
          Expanded(
            child: SizedBox(
              height: 50,
              child: FlatButton(
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(18)),
                color: Colors.orange,
                onPressed: (){
                  cart.addOrderItem(carts);
                  Navigator.of(context).push(MaterialPageRoute(builder: (_)=>CheckOutPage()));
                },
                child: Text(
                  "Buy Now",
                  style: TextStyle(
                      fontSize: 17,
                      fontWeight: FontWeight.bold, color: Colors.black),
                ),

              ),
            ),
          )
        ],
      ),
    );
  }
}
