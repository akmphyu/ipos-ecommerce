import 'package:flutter/material.dart';
import 'package:ipos_shop/models/Product.dart';
import 'package:ipos_shop/constants.dart';

class ColorAndSize extends StatelessWidget {
  final String weight;
  final String length;
  final String width;
  final String height;
  ColorAndSize({this.weight,this.length,this.width,this.height});
  @override
  Widget build(BuildContext context) {
    return  Row(
      children: [
        Expanded(
          child: RichText(
            text: TextSpan(
              style: TextStyle(color: kTextColor),
              children: [
                TextSpan(text: "Weight\n"),
                TextSpan(
                    text: "${weight} kg"),
              ],
            ),
          ),
        ),
        Expanded(
          child: RichText(
            text: TextSpan(
              style: TextStyle(color: kTextColor),
              children: [
                TextSpan(text: "Length\n"),
                TextSpan(
                  text: "${length} cm"),
              ],
            ),
          ),
        ),
        Expanded(
          child: RichText(
            text: TextSpan(
              style: TextStyle(color: kTextColor),
              children: [
                TextSpan(text: "Width\n"),
                TextSpan(
                    text: "${width} cm"),
              ],
            ),
          ),
        ),
        Expanded(
          child: RichText(
            text: TextSpan(
              style: TextStyle(color: kTextColor),
              children: [
                TextSpan(text: "Height\n"),
                TextSpan(
                    text: "${height} cm"),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
class ColorDot extends StatelessWidget {
  final Color color;
  final bool isSelected;

  ColorDot({this.color, this.isSelected = false});
  @override
  Widget build(BuildContext context) {
    return Container(
      margin:
      EdgeInsets.only(top: kDefaultPaddin / 4, right: kDefaultPaddin / 2),
      padding: EdgeInsets.all(2.5),
      height: 24,
      width: 24,
      decoration: BoxDecoration(
        border: Border.all(color: isSelected ? color : Colors.transparent),
      ),
      child: DecoratedBox(
        decoration: BoxDecoration(color: color, shape: BoxShape.circle),
      ),
    );
  }
}
