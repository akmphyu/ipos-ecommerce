import 'package:flutter/material.dart';
import 'package:ipos_shop/models/Product.dart';
import 'package:ipos_shop/constants.dart';
import 'product_title_with_image.dart';
import 'color_and_size.dart';
import 'description.dart';
import 'cart_counter.dart';
import 'package:flutter_svg/svg.dart';
import 'add_to_cart.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import "../../../providers/cart.dart";

class Body extends StatefulWidget {
  final String product;

  Body({this.product});

  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  Map<String, dynamic> products;
  List<dynamic> productList;

  getData(id) async{
    http.Response response = await http
        .get(Uri.encodeFull("http://uat.ipos.winspecgroup.com/v2/product/get-product-by-name?id=${id}"),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer YmVhcmVyIGpqaHNqZ2hram9zcGdmc2hpeUFHREZJSE1TS0JOSkJTRFZQQUtPRVI1NDg1'
        }

    );
    this.setState((){
      products = json.decode(response.body);
      productList = products['data'];
    });

  }

  @override
  void initState() {
    // TODO: implement initState
    this.getData(widget.product);
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    String productPrice = productList[0]['product_price'] == null ? '100' : productList[0]['product_price'];
    return SingleChildScrollView(
      child: Column(
        children: [
          SizedBox(
            height: size.height,
            child: Stack(
              children: [
                Container(
                  margin: EdgeInsets.only(top: size.height * 0.3),
                  padding: EdgeInsets.only(
                      top: size.height * 0.12,
                      left: kDefaultPaddin,
                      right: kDefaultPaddin),
//                  height: 500,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(24),
                        topRight: Radius.circular(24)),
                  ),
                  child: Column(
                    children: [
                      ColorAndSize(
                        weight: productList[0]['base_uom_weight_kg'],
                        length: productList[0]['base_uom_length_meter'],
                        width: productList[0]['base_uom_width_meter'],
                        height: productList[0]['base_uom_height_meter'],
                      ),
                      SizedBox(height: kDefaultPaddin/2,),
                      description(product: productList[0]['description']),
                      SizedBox(height: kDefaultPaddin/2,),
                      CartCounter(),
                      SizedBox(height: kDefaultPaddin/2,),
                      add_to_cart(carts: CartItem(productList[0]['product_code'],productList[0]['description'],1, double.parse(productPrice)))
                    ],
                  ),
                ),
                ProductTitleWithImage(
                  productBrand: productList[0]['brand_name'],
                  productCode: productList[0]['product_code'],
                  productCurrency: productList[0]['currency'] == null ? '\$' : productList[0]['currency'],
                  productPrice: productPrice,
                ), //insert here
              ],
            ),
          )
        ],
      ),
    );
  }
}
