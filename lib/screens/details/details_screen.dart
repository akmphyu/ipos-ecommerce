import 'package:flutter/material.dart';
import 'package:ipos_shop/models/Product.dart';
import 'package:flutter_svg/svg.dart';
import 'package:ipos_shop/constants.dart';
import 'components/body.dart';
import 'package:provider/provider.dart';
import '../../providers/cart.dart';
import '../../widgets/badge.dart';
import '../../screens/customer/customer_orders.dart';
import "../../screens/cart/product.dart";

class DetailsScreen extends StatefulWidget {
  final String product;
  DetailsScreen({this.product});

  @override
  _DetailsScreenState createState() => _DetailsScreenState();
}

class _DetailsScreenState extends State<DetailsScreen> {

  @override
  Widget build(BuildContext context) {
    final carts = Provider.of<Cart>(context).orderItems;
    return Scaffold(
      //each product have a color
      backgroundColor: Colors.orange,
      appBar: AppBar(title: Text('iPOS Shop'),backgroundColor: Colors.orange, actions: [
        Consumer<Cart>(
            builder: (_,cart,ch) => Badge(
              child: ch,
              value: cart.orderItemCount.toString(),
            ),
            child: IconButton(
              icon: Icon(Icons.shopping_cart),
              onPressed: (){
                Navigator.of(context).push(MaterialPageRoute(builder: (_)=>CartProductScreen()));
              },
            )
        )
      ],),

      body: Body(product: widget.product),
    );
  }
}
AppBar buildAppBar(BuildContext context){
  return AppBar(
    backgroundColor: Colors.orange,
    elevation: 0,
    leading: IconButton(
      icon: SvgPicture.asset(
        'assets/icons/back.svg',
        color: Colors.white,
      ),
      onPressed: () => Navigator.pop(context),
    ),
    actions: [
      IconButton(
          icon: SvgPicture.asset('assets/icons/search.svg'),
          onPressed: () {}),
      IconButton(
          icon: SvgPicture.asset('assets/icons/cart.svg'), onPressed: () {}),
      SizedBox(width: kDefaultPaddin/2,),
    ],
  );
}