import 'package:flutter/material.dart';

class NoOrder extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Text("No Product in cart"),
    );
  }
}
