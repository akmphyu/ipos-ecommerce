import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'customer_order_screen.dart';
import '../../widgets/main_drawer.dart';

class CustomerOrders extends StatefulWidget {
  static const routeName = '/customer-orders';

  @override
  _CustomerOrdersState createState() => _CustomerOrdersState();
}

class _CustomerOrdersState extends State<CustomerOrders> {
  Map<String, dynamic> profiles;
  List<dynamic> profileList;
  List<dynamic> profileListData = [];

  getData() async {
    http.Response response = await http.get(
        Uri.encodeFull(
            "http://uat.ecommerce.winspecgroup.com/v2/customer-info/get-customer-info"),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer YmVhcmVyIGpqaHNqZ2hram9zcGdmc2hpeUFHREZJSE1TS0JOSkJTRFZQQUtPRVI1NDg1'
        });
    this.setState(() {
      profiles = json.decode(response.body);

      profileList = profiles['status'];
      if(profileList != null){
        profileListData.add(profileList);
      }
    });

  }

  @override
  void initState() {
    // TODO: implement initState

    this.getData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return DefaultTabController(
      length: 4,
      child: Scaffold(
        appBar: AppBar(
          title: Text('My Orders'),
          backgroundColor: Colors.orange,
          bottom: TabBar(

            tabs: [
              FittedBox(
                child: Tab(

                  text: 'All',
                ),
              ),
              FittedBox(child: Tab(text: 'Pending Picking')),
              FittedBox(
                child: Tab(
                  text: 'Released',
                ),
              ),
              FittedBox(
                child: Tab(
                  text: 'Delivered',
                ),
              )
            ],
          ),
        ),

        drawer: MainDrawer(),
        body: TabBarView(children: [
          CustomerOrderScreen(jobStatus: 'all',),
          CustomerOrderScreen(jobStatus: 'PROCESSING',),
          CustomerOrderScreen(jobStatus: 'SHIPPED',),
          CustomerOrderScreen(jobStatus: 'COMPLETED',),
        ],),
      ),
    );
  }
}
