import 'package:flutter/material.dart';
import '../../widgets/main_drawer.dart';
import 'package:ipos_shop/widgets/profileBanner.dart';
import 'myprofile_detail.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class CustomerProfile extends StatefulWidget {
  static const routeName = '/customer-profile';

  @override
  _CustomerProfileState createState() => _CustomerProfileState();
}

class _CustomerProfileState extends State<CustomerProfile> {
  Map<String, dynamic> profiles;
//  var profileList = {};


  getData() async {
    http.Response response = await http.get(
        Uri.encodeFull(
            "http://uat.ecommerce.winspecgroup.com/v2/customer-info/get-customer-info"),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer YmVhcmVyIGpqaHNqZ2hram9zcGdmc2hpeUFHREZJSE1TS0JOSkJTRFZQQUtPRVI1NDg1'
        });
    this.setState(() {
      profiles = json.decode(response.body);
//      profileList = profiles['data'];

    });

  }
  @override
  void initState() {
    // TODO: implement initState

    this.getData();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: mainBanner("My Profile"),
      drawer: MainDrawer(),
      body: MyProfileDetail(profiles),

    );
  }
}
