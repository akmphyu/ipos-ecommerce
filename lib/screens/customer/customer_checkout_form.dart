import 'package:flutter/material.dart';
import 'package:ipos_shop/widgets/banner.dart';

class CustomerCheckoutForm extends StatefulWidget {
  @override
  _CustomerCheckoutFormState createState() => _CustomerCheckoutFormState();
}

class _CustomerCheckoutFormState extends State<CustomerCheckoutForm> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController firstNameController = TextEditingController();
  TextEditingController lastNameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController countryController = TextEditingController();
  TextEditingController address1Controller = TextEditingController();
  TextEditingController address2Controller = TextEditingController();
  TextEditingController postalController = TextEditingController();

  Column FormField(String text, TextEditingController controller) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 12.0),
          child: Text(text),
        ),
        TextField(
          controller: controller,
        )
      ],
    );
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: mainBanner("Make Your Checkout Here"),
      body: Container(
        padding: EdgeInsets.all(10),
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              FormField("First Name",firstNameController),
              FormField("Last Name",lastNameController),
              FormField("Email Address",emailController),
              FormField("Phone Number",phoneController),
              FormField("Country",countryController),
              FormField("Address Line 1",address1Controller),
              FormField("Address Line 2",address2Controller),
              FormField("Postal Code",postalController)
            ],
          ),
        ),
      ),
    );
  }
}
