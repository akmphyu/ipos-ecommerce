import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';


class MyProfileDetail extends StatelessWidget {
  final Map<String, dynamic> profileList;
  MyProfileDetail(this.profileList);


  @override
  Widget build(BuildContext context) {

    return Card(
      elevation: 5,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(top: 10, bottom: 10),
            alignment: Alignment.center,
            child: CircleAvatar(
              radius: 30.0,
              child: Image.asset(
                'assets/images/avatar2.png',
                fit: BoxFit.cover,
              ),
            ),
          ),
          VendorText(
              'Customer', '${profileList['data']['first_name']} ${profileList['data']['last_name']}'),
          VendorText(
              'Email', '${profileList['data']['email']}'),
          VendorText('Address',
              '${profileList['data']['address_1']},${profileList['data']['postal']},${profileList['data']['state']},${profileList['data']['country']}'),
          VendorText('Phone', '${profileList['data']['phone_numer']}'),
        ],
      ),
//        ),
//      ),
    );


  }

}

class VendorText extends StatelessWidget {
  final String displayName;
  final String displayText;
  VendorText(this.displayName, this.displayText);
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      child: Row(
        children: <Widget>[
          Expanded(
            child: Text(
              '${displayName}:',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
          SizedBox(
            width: 5,
          ),
          Expanded(child: Text(displayText))
        ],
      ),
    );
  }
}
