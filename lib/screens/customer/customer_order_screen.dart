import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:ipos_shop/widgets/orderItem.dart';
class CustomerOrderScreen extends StatefulWidget {
  final String jobStatus;
  CustomerOrderScreen({this.jobStatus = "all"});
  @override
  _CustomerOrderScreenState createState() => _CustomerOrderScreenState();
}

class _CustomerOrderScreenState extends State<CustomerOrderScreen> {
  Map<String, dynamic> order;
  List<dynamic> orderList;




  getData(jobStatus) async {
    http.Response response = await http.get(
        Uri.encodeFull(
            "http://uat.ecommerce.winspecgroup.com/v2/customer-info/get-purchase-list?status=${jobStatus}"),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer YmVhcmVyIGpqaHNqZ2hram9zcGdmc2hpeUFHREZJSE1TS0JOSkJTRFZQQUtPRVI1NDg1'
        });
    this.setState(() {
      order = json.decode(response.body);
      orderList = order['data'];

    });
  }



  @override
  void initState() {
    // TODO: implement initState
    this.getData(widget.jobStatus);

    super.initState();
  }
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: ListView.builder(
        itemBuilder: (BuildContext context, int index) {
          return OrderItem(orderList[index]["Order_Info"],orderList[index]["Purchase_Items"]);
        },
        itemCount: orderList == null ? 0 : orderList.length,
      ),

    );
  }
}
