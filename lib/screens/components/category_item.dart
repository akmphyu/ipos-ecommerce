import 'package:flutter/material.dart';
import 'package:ipos_shop/constants.dart';
import 'item_cart.dart';
import '../details/details_screen.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'category_item.dart';

class CategoryItem extends StatefulWidget {
  final String selectedBusinessCode;
  CategoryItem({this.selectedBusinessCode});

  @override
  _CategoryItemState createState() => _CategoryItemState();
}

class _CategoryItemState extends State<CategoryItem> {
  Map<String, dynamic> products;
  List<dynamic> productList;

  getData(selectedBusinessCode) async{
    http.Response response = await http
        .get(Uri.encodeFull("http://uat.ipos.winspecgroup.com/v2/product/get-product?customer_code=${selectedBusinessCode}"),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer YmVhcmVyIGpqaHNqZ2hram9zcGdmc2hpeUFHREZJSE1TS0JOSkJTRFZQQUtPRVI1NDg1'
        }

    );
    this.setState((){
      products = json.decode(response.body);
      productList = products['data'];
    });

  }
  @override
  void initState() {
    // TODO: implement initState
    this.getData(widget.selectedBusinessCode);
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return   Expanded(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: kDefaultPaddin),
        child: GridView.builder(
            itemCount: productList == null? 0: productList.length,
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisSpacing: kDefaultPaddin,
                crossAxisCount: 2,
                mainAxisSpacing: kDefaultPaddin,
                childAspectRatio: 0.75),
            itemBuilder: (context, index) => ItemCard(
              productId: productList[index]['trx_no'],
              productCode: productList[index]['product_code'],
              productDescription: productList[index]['description'],
              press: () => Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => DetailsScreen(
                        product: productList[index]['trx_no'],
                      ))),
            )),
      ),
    );
  }
}
