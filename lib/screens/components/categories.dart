import 'package:flutter/material.dart';

import 'category_item.dart';
class Categories extends StatefulWidget {

  @override
  _CategoriesState createState() => _CategoriesState();
}
class _CategoriesState extends State<Categories> {
  List<String> categories = ["WMO", "AF", "WMO2"];
  int selectedIndex = 0;
  String selectedBusinessCode = 'WMO';

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          title: Text("iPOS-shop"),
          backgroundColor: Colors.orange,
          bottom: TabBar(

            tabs: [
              FittedBox(
                child: Tab(

                  text: 'WMO',
                ),
              ),
              FittedBox(child: Tab(text: 'AF')),
              FittedBox(
                child: Tab(
                  text: 'WMO2',
                ),
              ),

            ],
          ),
        ),


        body: TabBarView(children: [
          CategoryItem(selectedBusinessCode: 'WMO',),
          CategoryItem(selectedBusinessCode: '00033',),
          CategoryItem(selectedBusinessCode: 'WMO2',),

        ],),
      ),
    );
  }
  }

//  Widget buildCategory(int index) {
//    return GestureDetector(
//      onTap: (){
//        setState(() {
//          selectedIndex = index;
//          selectedBusinessCode = categories[selectedIndex];
//        });
//      },
//      child: Padding(
//        padding: const EdgeInsets.symmetric(horizontal: kDefaultPaddin),
//        child: Column(
//          crossAxisAlignment: CrossAxisAlignment.start,
//          children: [
//            Text(
//              categories[index],
//              style: TextStyle(
//                fontWeight: FontWeight.bold,
//                color: selectedIndex == index ? kTextColor : kTextLightColor,
//              ),
//            ),
//            Container(
//              margin: EdgeInsets.only(top: kDefaultPaddin / 4),
//              height: 2,
//              width: 30,
//              color: selectedIndex == index ? Colors.black : Colors.transparent,
//            ),
//          ],
//        ),
//      ),
//    );
//  }

