import 'package:flutter/material.dart';
import 'package:ipos_shop/models/Product.dart';
import 'package:ipos_shop/constants.dart';

class ItemCard extends StatelessWidget {
  final String productId;
  final String productDescription;
  final String productCode;

  final Function press;
  ItemCard({this.productId, this.productDescription,this.productCode,this.press});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: press,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Container(
              padding: EdgeInsets.all(kDefaultPaddin),
//          height: 180,
//          width: 160,
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(16)),
              child: Hero(tag: "${productCode}",child: Image.asset("assets/images/product_default.jpg")),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: kDefaultPaddin / 4),
            child: Text(
              productCode,
              style: TextStyle(color: kTextLightColor),
            ),
          ),
          Text(
            productDescription,
            style: TextStyle(fontWeight: FontWeight.bold),
          )
        ],
      ),
    );
  }
}
