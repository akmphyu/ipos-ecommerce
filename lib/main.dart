import 'package:flutter/material.dart';
import 'screens/home/home_screen.dart';
import 'constants.dart';
import 'screens/customer/customer_orders.dart';
import 'screens/customer/customer_profile.dart';
import 'screens/customer/myprofile_detail.dart';
import 'package:provider/provider.dart';
import 'providers/cart.dart';
import 'screens/customer/no_order.dart';
void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
//    final carts = Provider.of<Cart>(context).orderItems;
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (ctx) => Cart(),)
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'iPOS Shop',
        theme: ThemeData(
          textTheme: Theme.of(context).textTheme.apply(bodyColor: kTextColor),
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        initialRoute: '/',
        routes: {
          '/': (ctx) => HomeScreen(0),
          CustomerOrders.routeName: (ctx) => CustomerOrders(),
          CustomerProfile.routeName: (ctx) => CustomerProfile(),

        },
      ),
    );
  }
}
