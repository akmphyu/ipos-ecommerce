import 'package:flutter/material.dart';

class OrderItem extends StatelessWidget {
   final Map<String, dynamic> order_info;
   final List<dynamic> orders;
   OrderItem(this.order_info, this.orders);

  @override
  Widget build(BuildContext context) {
    print(orders);
    return Card(
      child: Container(
        padding: EdgeInsets.all(20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(bottom: 10),
              child: CellItem('${order_info['ref_no']}','${order_info['order_date']}'),
            ),
            Divider(),
//            SizedBox(
//              height: 100.0,
//              child:
//              ListView.builder(itemBuilder: (BuildContext context, int index){
//                return ProductDetailItem(prodCode: "${orders[index]['product_trx_no']}",prodDes: "${orders[index]['product_trx_no']}",qty: "${orders[index]['qty']}",uom: "1",);
//              },itemCount: orders == null ? 0: orders.length,
//
//              ),
//
//            ),
              for(var order in orders) ProductDetailItem(prodCode: "${order['product_trx_no']}",prodDes: "${order['product_trx_no']}",qty: "${order['qty']}",uom: "1",)


          ],
        ),
      ),
    );
  }
}
class CellItem extends StatelessWidget {
  final String ref_no;
  final String date;
  CellItem(this.ref_no, this.date);

  @override
  Widget build(BuildContext context) {
    return  Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Expanded(child: Text('${ref_no}', style: TextStyle(fontWeight: FontWeight.bold),)),
          Expanded(child: Text('${date}')),
        ],
      ),
    );
  }
}

class ProductDetailItem extends StatelessWidget {
  final String prodCode;
  final String prodDes;
  final String qty;
  final String uom;
  ProductDetailItem({this.prodCode,this.prodDes,this.qty,this.uom});
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 10),
      color: Colors.white70,
      child: Row(
        children: <Widget>[
          Container(
            width: 60,
            height: 80,
            child: Image.asset('assets/images/product_default.jpg'),
          ),
          Container(
            padding: EdgeInsets.only(left: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Text('Code:', style: TextStyle(fontWeight: FontWeight.bold),),
                    SizedBox(width: 10,),
                    Text(prodCode),
                  ],
                ),
                Row(
                  children: <Widget>[
                    Text('Description:', style: TextStyle(fontWeight: FontWeight.bold),),
                    SizedBox(width: 10,),
                    Text(prodDes),
                  ],
                ),

                Row(
                  children: <Widget>[
                    Text('Qty:', style: TextStyle(fontWeight: FontWeight.bold),),
                    SizedBox(width: 10,),
                    Text(qty),
                  ],
                ),
                Row(
                  children: <Widget>[
                    Text('UOM:', style: TextStyle(fontWeight: FontWeight.bold),),
                    SizedBox(width: 10,),
                    Text('2'),

                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
