import 'package:flutter/material.dart';

AppBar mainBanner(String title){
  return AppBar(
    title: Text(title),
    backgroundColor: Colors.orange,
    actions: [
//        Consumer<Cart>(
//          builder: (_,cart,ch) => Badge(
//            child: ch,
//            value: cart.orderItemCount.toString(),
//          ),
//          child: IconButton(
//            icon: Icon(Icons.shopping_cart),
//            onPressed: (){
//              Navigator.of(context).push(MaterialPageRoute(builder: (_)=>CustomerOrders()));
//            },
//          )
//        )
      PopupMenuButton(
          icon: Icon(Icons.more_vert),
          itemBuilder: (_) =>
          [PopupMenuItem(child: Text('Logout'), value: 0)])
    ],
  );
}