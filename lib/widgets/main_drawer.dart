import 'package:flutter/material.dart';
import '../screens/customer/customer_orders.dart';
import '../screens/customer/customer_profile.dart';

class MainDrawer extends StatelessWidget {
  Widget buildListTile(String title,IconData icon, Function tapHandler){
    return ListTile(
      leading: Icon(icon,size: 26,),
      title: Text(title,style: TextStyle(fontWeight: FontWeight.bold, fontFamily: 'Raleway',fontSize: 20),),
      onTap: tapHandler,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: <Widget>[
          Container(
            height: 120,
            width: double.infinity,
            padding: EdgeInsets.all(20),
            alignment: Alignment.centerLeft,
            color: Colors.white,
            child: Image(image: AssetImage('assets/images/Winspec-logo.png'),),
          ),
          SizedBox(height: 20,),

          buildListTile('Profile', Icons.calendar_today,(){
//            Navigator.of(context).pushReplacementNamed('/',arguments: 2);
            Navigator.of(context).pushReplacementNamed(CustomerProfile.routeName);
          }),
          buildListTile('Orders', Icons.calendar_today,(){
            Navigator.of(context).pushReplacementNamed(CustomerOrders.routeName);
          }),

        ],
      ),
    );
  }
}
