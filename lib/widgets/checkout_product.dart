import 'package:flutter/material.dart';
import '../screens/details/components/cart_counter.dart';
import 'package:provider/provider.dart';
import "../providers/cart.dart";

class CheckoutProduct extends StatefulWidget {
  final String productCode;
  final String productDesc;
  final int productQty;
  final double productPrice;
  CheckoutProduct({this.productCode,this.productDesc,this.productQty,this.productPrice});

  @override
  _CheckoutProductState createState() => _CheckoutProductState();
}

class _CheckoutProductState extends State<CheckoutProduct> {
  @override
  Widget build(BuildContext context) {
    final carts = Provider.of<Cart>(context).orderItems;
    print(carts);
    return Card(
      child: Padding(

        padding: EdgeInsets.only(top: 10.0, bottom: 10.0, right: 10.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Image.asset(
              'assets/images/product_default.jpg',
              width: 80,
              height: 80,
            ), //add image location here
            Flexible(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text(
                    '${widget.productCode}',
                    style: TextStyle(fontSize: 18.0),
                    textAlign: TextAlign.start,
                  ),
                  Text(
                    '${widget.productDesc}',
                    style: TextStyle(fontSize: 15.0),
                    textAlign: TextAlign.start,
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [Text("\$ ${widget.productPrice}"),Text(' x ${widget.productQty}',)],
                  )
                ],
              ),
            ),
            Icon(
              Icons.delete,
              color: Colors.red,
            ),
          ],
        ),
      ),
    );
  }
}
