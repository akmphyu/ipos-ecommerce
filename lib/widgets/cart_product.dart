import 'package:flutter/material.dart';
import '../screens/details/components/cart_counter.dart';
import 'package:provider/provider.dart';
import "../providers/cart.dart";
import 'package:http/http.dart' as http;
import 'dart:convert';

class CartProduct extends StatefulWidget {
  final String productCode;
  final String productDesc;
  final int productQty;
  final bool checkbox;
  CartProduct({this.productCode,this.productDesc,this.productQty,this.checkbox});

  @override
  _CartProductState createState() => _CartProductState();
}

class _CartProductState extends State<CartProduct> {

  bool _checkbox =false;

  @override
  Widget build(BuildContext context) {

     if(widget.checkbox == true){
       _checkbox = true;
     }
//    _checkbox = widget.checkbox;
    return Card(
      child: Padding(
        padding: EdgeInsets.only(top: 15.0, bottom: 15.0, right: 10.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Checkbox(
              value: _checkbox,
              onChanged: (bool value) {
                setState(() {
                  _checkbox = !_checkbox;
                });
              },
            ),
            Image.asset(
              'assets/images/product_default.jpg',
              width: 80,
              height: 80,
            ), //add image location here
            Flexible(
              child: Column(
                children: <Widget>[
                  Text(
                    '${widget.productCode}',
                    style: TextStyle(fontSize: 18.0),
                    textAlign: TextAlign.start,
                  ),
                  Text(
                      '${widget.productDesc}',
                    style: TextStyle(fontSize: 15.0),
                    textAlign: TextAlign.start,
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 5.0),
                  ),
                  CartCounter()
                ],
              ),
            ),
            Icon(
              Icons.delete,
              color: Colors.red,
            ),
          ],
        ),
      ),
    );
  }
}
