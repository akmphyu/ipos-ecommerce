import 'package:flutter/material.dart';

class CheckoutAddress extends StatelessWidget {
  Map<String, dynamic> profiles;

  CheckoutAddress({ this.profiles});
  @override
  Widget build(BuildContext context) {

    return Row(children: [
      Flexible(
        child: Column(children: [
          Text("Delivery Address", style: TextStyle(fontWeight: FontWeight.bold),),
          Text('${profiles['data']['first_name']} ${profiles['data']['last_name']} | ${profiles['data']['phone_numer']}'),
          Text('${profiles['data']['address_1']},${profiles['data']['postal']},${profiles['data']['state']},${profiles['data']['country']}'),

        ],),
      )
    ],);
  }
}
