import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
class CartItem with ChangeNotifier {
  final String productCode;
  final String productDesc;
  final int productQty;
  final double productPrice;
  CartItem(this.productCode, this.productDesc,this.productQty,this.productPrice);
}
class Cart with ChangeNotifier {
 List<CartItem> _orderItems = [];


 List<CartItem> get orderItems{
   return [..._orderItems];
 }

 void addOrderItem(CartItem cartItem) {
   _orderItems.add(cartItem);
   notifyListeners();
 }

 int get orderItemCount{
   return _orderItems.length;
 }

 double get totalCost{
   double cost = 0.0;
   for(var i=0;i<_orderItems.length;i++){
     cost += _orderItems[i].productPrice * _orderItems[i].productQty;
   }
   return cost;
 }


 }

