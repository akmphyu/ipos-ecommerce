import 'package:flutter/material.dart';
import "package:flutter/foundation.dart";
import "Product.dart";

class Products with ChangeNotifier{
  List<Product> _products = [
    Product(
        id: 1,
        title: "Office Code",
        price: 234,
        size: 12,
        description: "This is Office Code",
        image: 'assets/images/bag_1.png',
        color: Color(0xFF3D82AE)
    ),
    Product(
        id: 2,
        title: "Belt Bag",
        price: 123,
        size: 12,
        description: "This is Office Code",
        image: 'assets/images/bag_2.png',
        color: Color(0xFF3D82AE)
    ),
    Product(
        id: 3,
        title: "Evening dress",
        price: 234,
        size: 12,
        description: "This is Office Code",
        image: 'assets/images/bag_3.png',
        color: Color(0xFF3D82AE)
    ),
  ];
  List<Product> get products{
    return [..._products];
  }
  Product findById(String id){
    return products.firstWhere((prod) => prod.id == id);
  }
  void addProduct(value){
    _products.add(value);
    notifyListeners();
  }
}