import 'package:flutter/material.dart';
import "package:flutter/foundation.dart";

class Product with ChangeNotifier{
  final String image, title, description;
  final int price,size,id;
  final Color color;
  Product({
   this.id,
   this.image,
   this.title,
   this.description,
   this.price,
   this.size,
   this.color
});

}

